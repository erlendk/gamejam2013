package gamejam2013.modes;

import gamejam2013.Actor;

class Result {
    Actor actor;
    int level;
    int score;

    public Result(Actor actor, int level, int score) {
        this.actor = actor;
        this.level = level;
        this.score = score;
    }
}
