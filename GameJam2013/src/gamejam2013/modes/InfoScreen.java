package gamejam2013.modes;

import static gamejam2013.Game.game;
import gamejam2013.Game;

public class InfoScreen extends GameMode {
	
	private GameMode currentMode;
	private GameMode previousMode;
	private String info;
	
	public InfoScreen(GameMode previousMode, String info) {
		currentMode = this;
		this.previousMode = previousMode;
		this.info = info;
	}

	@Override
	public GameMode draw() {
		game.background(0);
		game.textAlign(Game.CENTER);
        game.fill(255);
		game.text(info, Game.inst().width/2, Game.inst().height/2);
		
		GameMode tmpMode = currentMode;
		currentMode = this;
		return tmpMode;
	}

	@Override
	public void keyPressed() {
		switch(game.key) {
		case Game.INTERNAL_ESCAPE:
			currentMode = previousMode;
			break;
		}
	}
	
	@Override
	public boolean canBeQuit() {
		return false;
	}

}
