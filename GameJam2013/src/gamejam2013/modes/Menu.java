package gamejam2013.modes;

import static gamejam2013.Game.game;
import gamejam2013.Actor;
import gamejam2013.Game;

public class Menu extends GameMode {

	private GameMode currentMode;
	
	public Menu() {
		currentMode = this;
	}

	@Override
	public GameMode draw() {
		game.background(0);
		game.textAlign(Game.CENTER);
        game.fill(255);
		game.text(
				"The aMAZEing Escape\n\n" +
				"is brought to you\n\n" + "" +
				"by\n\n" +
				"Daniel Rødskog and Erlend Kristiansen\n\n\n\n" +
				"For controls, press 'c'\n\n" +
				"For game goal, press 'g'\n\n" +
				"To start, press ENTER\n\n\n\n\n" + 
				"initiated at\n\n" +
				"Game Jam 2013 @ sonen.ifi.uio.no",
				Game.inst().width/2, Game.inst().height/3);
		
		GameMode tmpMode = currentMode;
		currentMode = this;
		return tmpMode;
	}

	@Override
	public void keyPressed() {
		switch(game.keyCode) {
		case Game.ENTER:
		case Game.RETURN:
			currentMode = new Board(Game.gridSizeX, Game.gridSizeY, new Actor(), 1);
			break;
		}
		
		switch(game.key) {
		case 'c':
			currentMode = new InfoScreen(this,
					"Controls:\n\n\n" +
					"Use arrows to move player and selection\n\n" +
					"Numbers [1-9] to select tool\n\n" +
					"ENTER to apply tool on selected block\n\n" +
					"ESCAPE is your best friend");
			break;
		case 'g':
			currentMode = new InfoScreen(this, 
					"Goal:\n\n" + 
					"The goal of the game is to move the player to the ESCAPE button!");
			break;
		}
	}

}
