package gamejam2013.modes;
import static gamejam2013.Game.game;

import gamejam2013.Actor;
import gamejam2013.Game;
import gamejam2013.grid.Cell;
import gamejam2013.grid.Grid;
import gamejam2013.tools.BombTool;
import gamejam2013.tools.LeftRotateTool;
import gamejam2013.tools.RightRotateTool;

import java.util.Random;

public class Board extends GameMode {
    
    public static boolean inToolMode = false;

    private int gridSizeX;
    private int gridSizeY;
    private boolean levelIsCompleted;
    
    private Grid grid;
    private Actor actor;

    private int level;
    
    public Board(int gridSizeX, int gridSizeY, Actor actor, int level) {
        this.gridSizeX = gridSizeX;
        this.gridSizeY = gridSizeY;
        this.grid = new Grid(this.gridSizeX, this.gridSizeY);
        this.actor = actor;
        this.level = level;
        actor.setBoard(this);
        actor.resetToolbar();
        populateCells();
        actor.setCell(grid.get(0,0));
        giveActorTools();
    }

    public GameMode draw() {
        if (levelIsCompleted) {
            return new WinScreen(new Result(actor, level, 190));
        } else {
            Game.inst().background(255);
            grid.draw();
            actor.draw();
            return this;
        }
    }

    public void keyPressed() {
        switch (Game.inst().key) {
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            Board.inToolMode = actor.initSelection(game.key);
            break;
        case 0: // Set in Game, 0 == ESC
            Board.inToolMode = false;
            break;
        }

        actor.keyPressed();
    }
    
    void populateCells() {
        Random random = new Random();
        Cell goal = grid.get(Game.gridSizeX-1, random.nextInt(Game.gridSizeY));

        goal.makeMazeGoal();

        for (Cell[] col: grid.getCells()) {
            for (Cell c: col) {
                if (c.getBaseBlock() == null) 
                    c.makeMaze(null, Game.UP,false);
            }
        }       

        for (Cell cell : grid.getCells()[0]) {
            if (cell.connectedToGoal) {
                actor.setCell(cell);
            }
        }

        for (Cell[] col: grid.getCells()) {
            for (Cell c: col) {
                if (new Random().nextInt(100) < 10* Game.sqrt(level*10));
                    c.scramble();
            }
        }       
    }

    public void giveActorTools() {
        actor.giveTool(new LeftRotateTool(10));
        actor.giveTool(new RightRotateTool(12));
        actor.giveTool(new BombTool(5));
    }

    public void levelCompleted() {
        levelIsCompleted = true;
    }
    
    @Override
    public boolean canBeQuit() {
    	return !inToolMode;
    }

}
