package gamejam2013.modes;

import static gamejam2013.Game.game;
import gamejam2013.Game;

class WinScreen extends GameMode {
    boolean display = true;
    Result result;

    public WinScreen(Result result) {
        this.result = result;
    }


    public void keyPressed() {
        display = false;
    }

    public GameMode draw() {
        if (display) {
            Game.inst().background(0);
            Game.inst().textAlign(Game.CENTER);
            game.fill(255);
            Game.inst().text("LEVEL "+ result.level +" COMPLETE\nPress Enter to continue",
                    Game.inst().width/2, Game.inst().height/2);

            return this;

        } else
            return new Board(Game.gridSizeX, Game.gridSizeY, result.actor, result.level+1);
    }

}
