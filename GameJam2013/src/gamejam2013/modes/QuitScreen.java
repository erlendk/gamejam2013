package gamejam2013.modes;

import static gamejam2013.Game.game;
import gamejam2013.Game;

public class QuitScreen extends GameMode {

	private boolean display;
	private GameMode currentMode;

	public QuitScreen(GameMode currentMode) {
		this.display = true;
		this.currentMode = currentMode;
	}

	@Override
	public GameMode draw() {
		if (display) {
			Game.inst().background(0);
			Game.inst().textAlign(Game.CENTER);
			game.fill(255);
			Game.inst().text("Press 'q' to quit", Game.inst().width/2, Game.inst().height/2);
			return this;
		}
		return currentMode;
	}

	@Override
	public void keyPressed() {
		switch (game.key) {
		case 'q':
			System.exit(0);
		case 0:
			display = false;
			break;
		}
	}
	@Override
	public boolean canBeQuit() {
		return false;
	}

}
