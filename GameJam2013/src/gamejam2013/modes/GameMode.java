package gamejam2013.modes;

public abstract class GameMode {

	public GameMode() {
		super();
	}

	public abstract GameMode draw();
	public abstract void keyPressed();

	public boolean canBeQuit() {
		return true;
	}

}