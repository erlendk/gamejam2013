package gamejam2013;

import gamejam2013.modes.GameMode;
import gamejam2013.modes.Menu;
import gamejam2013.modes.QuitScreen;

import java.util.HashMap;

import processing.core.PApplet;
import processing.core.PImage;
import ddf.minim.AudioPlayer;
import ddf.minim.Minim;


@SuppressWarnings("serial")
public class Game extends PApplet {

	public static final char INTERNAL_ESCAPE = 0;
	public static final int pixelsPerCell = 60;
	public static final int gridSizeX = 15;
	public static final int gridSizeY = 10;

	public static PApplet game;

	private static ImageManager imageManager;
	private static SoundManager soundManager;

	private GameMode currentMode;

	public void setup() {
		size(gridSizeX*pixelsPerCell, (1+gridSizeY)*pixelsPerCell);
		frameRate(30);
		game = this;
		imageManager = new ImageManager();
		soundManager = new SoundManager(this);
		currentMode = new Menu();
	}

	public void draw() {
		currentMode = currentMode.draw();
	}

	public static PApplet inst() {
		return game;
	}

	@Override
	public void keyPressed() {
		switch (key) {
		case ESC:
			key = INTERNAL_ESCAPE;
			if (currentMode.canBeQuit()) {
				currentMode = new QuitScreen(currentMode);
				return;
			}
			break;
		}
		currentMode.keyPressed();
	}

	public static ImageManager imageManager() {
		return imageManager;
	}

	public static SoundManager soundManager() {
		return soundManager;
	}

	public class ImageManager {

		private HashMap<String, PImage> images = new HashMap<String, PImage>();

		public PImage getImage(String imageName) {
			if (!images.containsKey(imageName))
				images.put(imageName, loadImage(imageName));
			return images.get(imageName);
		}

	}

	public class SoundManager {
		Minim minim; 

		public SoundManager(PApplet game) {
			minim = new Minim(game);
		}

		private HashMap<String, AudioPlayer> sounds = new HashMap<String, AudioPlayer>();
		private AudioPlayer currentPlayer;

		public AudioPlayer getPlayer(String soundName) {
			if (currentPlayer != null)
				currentPlayer.pause();

			if (!sounds.containsKey(soundName))
				sounds.put(soundName, minim.loadFile(soundName));
			currentPlayer = sounds.get(soundName);
			return currentPlayer;
		}
	}

	public static void main(String args[]) {
		PApplet.main("gamejam2013.Game", args );
	}
}
