package gamejam2013.tools;

import gamejam2013.blocks.XBlock;
//import static gamejam2013.Game.game;
import gamejam2013.grid.Cell;

public class BombTool extends Tool {

	public BombTool(int amount) {
		super("bomb.png", amount);
	}

	@Override
	protected void apply(Cell cell) {
		// TODO: make animation
//		game.image(Game.imageManager().getImage("explosion.png"), cell.x, cell.y, Game.pixelsPerCell, Game.pixelsPerCell);
		cell.setBaseBlock(new XBlock());
	}

}
