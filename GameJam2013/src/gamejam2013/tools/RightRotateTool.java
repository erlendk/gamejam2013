package gamejam2013.tools;

import gamejam2013.grid.Cell;


public class RightRotateTool extends Tool {

    public RightRotateTool(int amount) {
        super("rotateright.png", amount);
    }

	@Override
	public void apply(Cell cell) {
		cell.rotateBlockRight();
	}
}

