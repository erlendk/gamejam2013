package gamejam2013.tools;
import static gamejam2013.Game.game;

import gamejam2013.Game;

import java.util.List;
import java.util.LinkedList;

public class Toolbar {
    List<Tool> tools;
	private int selectedTool;

    public Toolbar() {
        tools = new LinkedList<Tool>();
        selectedTool = 0;
    }

    public void draw() {
        float x = 0;
        float y = game.height - Game.pixelsPerCell;

        int i = 1;
        for (Tool tool : tools) {
            tool.draw(x,y,i, i == selectedTool);
            i++;
            x+= Game.pixelsPerCell;
        }

    }

    public void addTool(Tool t) {
        tools.add(t);
    }

	public void selectTool(char key) {
        if (containsTool(key))
            selectedTool = getToolNumber(key);
	}

	public Tool getSelectedTool() {
		if (getToolIndex(selectedTool) < 0)
			return null;
		return tools.get(getToolIndex(selectedTool));
	}
	
	private int getToolIndex(int selectedTool) {
		return selectedTool - 1;
	}

	public void dropSelection() {
		selectedTool = -1;
	}

	public boolean containsTool(char key) {
        return (tools.size() >= getToolNumber(key) && getToolNumber(key) > 0);
	}

	private int getToolNumber(char key) {
		return key - '0';
	}
}
