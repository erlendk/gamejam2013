package gamejam2013.tools;

import gamejam2013.grid.Cell;


public class LeftRotateTool extends Tool {

    public LeftRotateTool(int amount) {
        super("rotateleft.png", amount);
    }

	@Override
	public void apply(Cell cell) {
		cell.rotateBlockLeft();
	}
}
