package gamejam2013.tools;

import static gamejam2013.Game.game;
import gamejam2013.Game;
import gamejam2013.grid.Cell;

public abstract class Tool {
	String iconName;
	int amount;

	public Tool(String iconName, int amount) {
		this.iconName = iconName;
		this.amount = amount;
	}

	public void draw(float x, float y, int i, boolean isSelected) {
		if (isSelected) {
			game.fill(0, 255, 0, 100);
			game.rect(x, y, Game.pixelsPerCell, Game.pixelsPerCell);
		}
		game.fill(0);
		game.image(Game.imageManager().getImage(iconName), x, y, Game.pixelsPerCell, Game.pixelsPerCell);
		game.textAlign(Game.LEFT, Game.TOP);
		game.text(i, x+2, y+2);
		game.textAlign(Game.RIGHT, Game.BOTTOM);
		game.text(amount, x - 2 + Game.pixelsPerCell, y - 2 + Game.pixelsPerCell);
	}

	public void applyOn(Cell cell) {
		if (amount <= 0)
			return;
		
		apply(cell);
		amount--;
	}

	protected abstract void apply(Cell cell);
	
}
