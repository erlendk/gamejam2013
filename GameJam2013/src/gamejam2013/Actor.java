package gamejam2013;

import static gamejam2013.Game.game;
import static gamejam2013.modes.Board.inToolMode;
import gamejam2013.grid.Cell;
import gamejam2013.modes.Board;
import gamejam2013.tools.Tool;
import gamejam2013.tools.Toolbar;


public class Actor {

    private Cell cell;
    private Cell selectedCell;
    private Board board;

    private Toolbar toolbar;

    public Actor() {
        resetToolbar();
    }

    public void resetToolbar() {
        toolbar = new Toolbar();
    }

    public void draw() {
        game.fill(0);
        game.ellipse(
                cell.x + Game.pixelsPerCell/2.0f, 
                cell.y + Game.pixelsPerCell/2.0f, 
                Game.pixelsPerCell/3.0f, 
                Game.pixelsPerCell/3.0f);

        if (!Board.inToolMode)
            toolbar.dropSelection();

        toolbar.draw();
    }

    public void keyPressed() {
        char key;
        switch (game.keyCode) {
            case Game.UP:
                key = 'w';
                break;
            case Game.DOWN:
                key = 's';
                break;
            case Game.LEFT:
                key = 'a';
                break;
            case Game.RIGHT:
                key = 'd';
                break;
            default:
                key = game.key;
        } 

        switch(key) {
            case 'w':
                if (inToolMode)
                    moveSelectionUp();
                else
                    moveActorUp();
                break;
            case 'd':
                if (inToolMode)
                    moveSelectionRight();
                else
                    moveActorRight();
                break;
            case 's':
                if (inToolMode)
                    moveSelectionDown();
                else
                    moveActorDown();
                break;
            case 'a':
                if (inToolMode)
                    moveSelectionLeft();
                else
                    moveActorLeft();
                break;
            case 'r':
                if (!inToolMode)
                    break;
                selectedCell.rotateBlockRight();
                break;
            case 't':
                if (!inToolMode)
                    break;
                selectedCell.rotateBlockLeft();
                break;
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                toolbar.selectTool(game.key);
                break;
            case Game.ENTER:
            case Game.RETURN:
            	if (!inToolMode)
                    break;
            	applySelectedTool();
                break;
        }

    }

    private void applySelectedTool() {
        toolbar.getSelectedTool().applyOn(selectedCell);
        toolbar.dropSelection();
        selectedCell.deselect();
        inToolMode = false;
    }

    public void moveSelectionRight(){
        if (selectedCell.right != null && (selectedCell == cell || selectedCell.right == cell)) {
            selectedCell.deselect();
            selectedCell = selectedCell.right;
            selectedCell.select();
        }
    }

    public void moveSelectionDown(){
        if (selectedCell.down != null && (selectedCell == cell || selectedCell.down == cell)) {
            selectedCell.deselect();
            selectedCell = selectedCell.down;
            selectedCell.select();
        }
    }

    public void moveSelectionLeft(){
        if (selectedCell.left != null && (selectedCell == cell || selectedCell.left == cell)) {
            selectedCell.deselect();
            selectedCell = selectedCell.left;
            selectedCell.select();
        }
    }

    public void moveSelectionUp(){
        if (selectedCell.up != null && (selectedCell == cell || selectedCell.up == cell)) {
            selectedCell.deselect();
            selectedCell = selectedCell.up;
            selectedCell.select();
        }
    }

    public void moveActorUp() {
        if (actorCanMoveIntoCell(cell.up, Game.UP, Game.DOWN)) {
            cell = cell.up;
            cell.trigger(board);
        }
    }

    public void moveActorRight() {
        if (actorCanMoveIntoCell(cell.right, Game.RIGHT, Game.LEFT)) {
            cell = cell.right;
            cell.trigger(board);
        }
    }

    public void moveActorDown() {
        if (actorCanMoveIntoCell(cell.down, Game.DOWN, Game.UP)) {
            cell = cell.down;
            cell.trigger(board);
        }
    }

    public void moveActorLeft() {
        if (actorCanMoveIntoCell(cell.left, Game.LEFT, Game.RIGHT)) {
            cell = cell.left;
            cell.trigger(board);
        }
    }

    private boolean actorCanMoveIntoCell(Cell otherCell, int toDirection, int fromDirection) {
        return otherCell != null && cell.actorCanMoveIntoFrom(toDirection) && otherCell.actorCanMoveIntoFrom(fromDirection);
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public Cell getCell() {
        return cell;
    }

    public void setSelectedCell(Cell cell) {
        selectedCell = cell;
    }       

    /**
     * @param board the board to set
     */
    public void setBoard(Board board) {
        this.board = board;
    }

    public boolean initSelection(char key) {
        if (!Board.inToolMode) {
            setSelectedCell(cell);
            selectedCell.select();
        }

        return toolbar.containsTool(key);

    }

    public void giveTool(Tool t) {
        toolbar.addTool(t);
    }


}
