package gamejam2013.grid;

import gamejam2013.Game;
import gamejam2013.blocks.BaseBlock;
import gamejam2013.blocks.CornerBlock;
import gamejam2013.blocks.EndBlock;
import gamejam2013.blocks.GoalBlock;
import gamejam2013.blocks.SolidBlock;
import gamejam2013.blocks.StraightBlock;
import gamejam2013.blocks.TBlock;
import gamejam2013.blocks.XBlock;
import gamejam2013.modes.Board;

import java.util.Random;
import java.util.List;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.Collections;

import static gamejam2013.Game.game;
import static gamejam2013.modes.Board.inToolMode;

public class Cell {

	private BaseBlock baseBlock;
	public float x;
	public float y;
    boolean visited;
    public boolean connectedToGoal;
    Cell previous;

	public Cell up;
	public Cell down;
	public Cell right;
	public Cell left;
	private boolean isSelected; 
	
	public Cell(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public void setBaseBlock(BaseBlock baseBlock) {
		this.baseBlock = baseBlock;
	}


	public void draw() {
		if (!inToolMode)
			isSelected = false;
		baseBlock.draw(x, y);
		if (isSelected)
			showToolOverlay();
		game.noFill();
		game.stroke(0, 100);
		game.rect(x, y, Game.pixelsPerCell, Game.pixelsPerCell);
//		game.point(x + Game.pixelsPerCell, y);
//		game.point(x + Game.pixelsPerCell, y + Game.pixelsPerCell);
//		game.point(x, y + Game.pixelsPerCell);
	}

	private void showToolOverlay() {
		Game.inst().fill(0, 0, 255, 100);
		Game.inst().rect(x, y, Game.pixelsPerCell, Game.pixelsPerCell);
	}

    /* What happens when actor moves into cell */
    public void trigger(Board board) {
        baseBlock.trigger(board);
    }


	public void setCellUp(Cell cell) {
		this.up = cell;
	}

	public void setCellDown(Cell cell) {
		this.down = cell;
	}

	public void setCellRight(Cell cell) {
		this.right = cell;
	}

	public void setCellLeft(Cell cell) {
		this.left = cell;
	}

	public boolean actorCanMoveIntoFrom(int fromDirection) {
		return baseBlock.canMoveIntoFrom(fromDirection);
	}

	public void rotateBlockRight() {
		baseBlock.rotateRight();
	}

	public void rotateBlockLeft() {
		baseBlock.rotateLeft();
	}
		
    public void select() {
		isSelected = true;
	}

    public void deselect() {
        isSelected = false;
    }

	enum BlockFactory {
		INSTANCE;
		
		List<BaseBlock> blocks = new LinkedList<BaseBlock>();
		
		private BlockFactory() {
			blocks.add(new CornerBlock());
			blocks.add(new StraightBlock());
			blocks.add(new TBlock());
			blocks.add(new XBlock());
		}

		public BaseBlock getRandomBlock() {
			Collections.shuffle(blocks);
			return blocks.get(0).newInstance();
		}
		
	}

    public BaseBlock getBaseBlock() {
        return baseBlock;
    }


    public void makeMazeGoal() {
        setBaseBlock(new GoalBlock());
        visited = true;

        if (up != null)
            up.makeMaze(this,Game.DOWN,true);
        if (down != null)
            down.makeMaze(this,Game.UP,true);
        if (left != null)
            left.makeMaze(this,Game.RIGHT,true);
    }

    public void makeMaze(Cell previous, int backDirection, boolean connectedToGoal) {
        if (visited)
            return;

        this.connectedToGoal = connectedToGoal;
        visited = true;

        this.previous = previous;

        setBaseBlock(BlockFactory.INSTANCE.getRandomBlock());

        while (!actorCanMoveIntoFrom(backDirection))
            rotateBlockRight();

        List<Integer> directions = Arrays.asList( Game.UP, Game.RIGHT, Game.LEFT, Game.DOWN );
        Collections.shuffle(directions);

        for (int dir : directions) {
            switch (dir) {
                case Game.UP:
                    if (up == null || !actorCanMoveIntoFrom(dir))
                        break;
                    up.makeMaze(this, Game.DOWN,connectedToGoal);
                    break;

                case Game.DOWN:
                    if (down == null || !actorCanMoveIntoFrom(dir))
                        break;
                    down.makeMaze(this, Game.UP,connectedToGoal);
                    break;

                case Game.LEFT:
                    if (left == null || !actorCanMoveIntoFrom(dir))
                        break;
                    left.makeMaze(this, Game.RIGHT,connectedToGoal);
                    break;

                case Game.RIGHT:
                    if (right == null || !actorCanMoveIntoFrom(dir))
                        break;
                    right.makeMaze(this, Game.LEFT,connectedToGoal);
                    break;
            }
        } 

    }

    public void scramblePath() {

    }

    public void scramble() {
        if (containsGoal())
            return;

        switch(new Random().nextInt(4)) {
            case 0:
                rotateBlockRight();
                break;
            case 1:
                rotateBlockLeft();
                break;
            case 2:
                setBaseBlock(new SolidBlock());
                break;
            case 3:
                setBaseBlock(new EndBlock());
                break;
        }
    }

    boolean containsGoal() {
        return baseBlock.isGoal();
    }

}
