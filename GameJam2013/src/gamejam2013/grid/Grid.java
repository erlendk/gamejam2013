package gamejam2013.grid;

import gamejam2013.Game;

public class Grid {
	private Cell[][] cells;

	public Grid(int gridSizeX, int gridSizeY) {
		this.cells = new Cell[gridSizeX][gridSizeY];
		initialize(gridSizeX, gridSizeY);
	}

	private void initialize(int gridSizeX, int gridSizeY) {
		createCells(gridSizeX, gridSizeY);
		setCellNeighbors(gridSizeX, gridSizeY);
	}

	private void setCellNeighbors(int gridSizeX, int gridSizeY) {
		for (int i = 0; i < gridSizeX; i++) {
			for (int j = 0; j < gridSizeY; j++) {
				if (j > 0)
					getCells()[i][j].setCellUp(getCells()[i][j-1]);
				if (j+1 < gridSizeY)
					getCells()[i][j].setCellDown(getCells()[i][j+1]);
				if (i+1 < gridSizeX)
					getCells()[i][j].setCellRight(getCells()[i+1][j]);
				if (i > 0)
					getCells()[i][j].setCellLeft(getCells()[i-1][j]);
			}
		}
	}

	private void createCells(int gridSizeX, int gridSizeY) {
		for (int i = 0; i < gridSizeX; i++) {
			for (int j = 0; j < gridSizeY; j++) {
				getCells()[i][j] = new Cell(i*Game.pixelsPerCell, j*Game.pixelsPerCell);
			}
		}
	}

	public void draw() {
		for (Cell[] col: getCells()) {
			for (Cell c: col) {
				c.draw();
			}
		}
	}

	public void keyPressed() {
	}

	public Cell[][] getCells() {
		return cells;
	}

    public Cell get(int x, int y) {
        return cells[x][y];
    }

}
