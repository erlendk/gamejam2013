package gamejam2013.blocks;

import gamejam2013.Game;
import gamejam2013.grid.Edge;

public class SolidBlock extends ImageBlock {
	
	public SolidBlock() {
		super("solid.png");
	}

	@Override
	public boolean canMoveIntoFrom(int fromDirection) {
		return false;
	}

	@Override
	void populateEdges() {
		edges.put(Game.RIGHT, Edge.CLOSED);
		edges.put(Game.DOWN, Edge.CLOSED);
		edges.put(Game.LEFT, Edge.CLOSED);
		edges.put(Game.UP, Edge.CLOSED);
	}

}
