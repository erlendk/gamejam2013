package gamejam2013.blocks;

import gamejam2013.Game;
import gamejam2013.grid.Edge;

public class XBlock extends ImageBlock {
	public XBlock() {
		super("X.png");
	}

	@Override
	void populateEdges() {
		edges.put(Game.RIGHT, Edge.OPEN);
		edges.put(Game.DOWN, Edge.OPEN);
		edges.put(Game.LEFT, Edge.OPEN);
		edges.put(Game.UP, Edge.OPEN);	
	}

}
