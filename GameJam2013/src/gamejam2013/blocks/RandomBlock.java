package gamejam2013.blocks;

import gamejam2013.Game;

import java.util.Random;

public class RandomBlock extends BaseBlock {
	
	private boolean randomValue;

	public RandomBlock() {
		this.randomValue = new Random().nextBoolean();
	}

	@Override
	public boolean canMoveIntoFrom(int fromDirection) {
		return this.randomValue;
	}

	public void draw(float x, float y) {
		if (randomValue)
			Game.inst().fill(255);
		else
			Game.inst().fill(0);
		Game.inst().rect(x, y, Game.pixelsPerCell, Game.pixelsPerCell);
	}

}
