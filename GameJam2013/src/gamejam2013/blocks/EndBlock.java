package gamejam2013.blocks;

import gamejam2013.Game;
import gamejam2013.grid.Edge;

public class EndBlock extends ImageBlock {
	public EndBlock() {
		super("end.png");
	}

	@Override
	void populateEdges() {
		edges.put(Game.RIGHT, Edge.CLOSED);
		edges.put(Game.DOWN, Edge.CLOSED);
		edges.put(Game.LEFT, Edge.OPEN);
		edges.put(Game.UP, Edge.CLOSED);		
	}

}
