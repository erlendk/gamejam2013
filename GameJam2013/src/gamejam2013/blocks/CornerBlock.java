package gamejam2013.blocks;

import gamejam2013.Game;
import gamejam2013.grid.Edge;

public class CornerBlock extends ImageBlock {
	
	public CornerBlock() {
		super("corner.png");
	}

	@Override
	void populateEdges() {
		edges.put(Game.RIGHT, Edge.CLOSED);
		edges.put(Game.DOWN, Edge.OPEN);
		edges.put(Game.LEFT, Edge.OPEN);
		edges.put(Game.UP, Edge.CLOSED);
	}

}
