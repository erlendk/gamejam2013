package gamejam2013.blocks;

import gamejam2013.modes.Board;

public abstract class BaseBlock {

	public abstract boolean canMoveIntoFrom(int fromDirection);
	public abstract void draw(float x, float y);
	
	public void rotateRight() {
		return;
	}
	
	public void rotateLeft() {
		return;
	}
	
	public BaseBlock newInstance() {
		try {
			return this.getClass().newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
	}

    public boolean isGoal() {
        return false;
    }

    public void trigger(Board board) {
    	return;
    }

}
