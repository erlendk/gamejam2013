package gamejam2013.blocks;

import gamejam2013.Game;
import gamejam2013.grid.Edge;
import gamejam2013.modes.Board;

public class GoalBlock extends ImageBlock {
	
	private static final String successSoundName = "success.wav";

    public GoalBlock() {
        super("escape.png");
        Game.soundManager().getPlayer(successSoundName);
    }

    void populateEdges() {
        edges.put(Game.RIGHT, Edge.OPEN);
        edges.put(Game.DOWN, Edge.OPEN);
        edges.put(Game.LEFT, Edge.OPEN);
        edges.put(Game.UP, Edge.OPEN);
    }

    @Override
    public boolean isGoal() {
        return true;
    }

    @Override
    public void trigger(Board board) {
    	Game.soundManager().getPlayer(successSoundName).play(0);
        board.levelCompleted();
    }

}
