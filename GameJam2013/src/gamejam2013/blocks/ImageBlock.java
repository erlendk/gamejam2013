package gamejam2013.blocks;

import gamejam2013.Game;
import gamejam2013.grid.Edge;

import java.util.HashMap;

public abstract class ImageBlock extends BaseBlock {
	
	private String imageName;
	private int rotationDegrees;
	protected HashMap<Integer, Edge> edges;
	
	abstract void populateEdges();
	
	protected ImageBlock(String imageName) {
		this.imageName = imageName;
		rotationDegrees = 0;
		edges = new HashMap<Integer, Edge>();
		populateEdges();
	}

	@Override
	public boolean canMoveIntoFrom(int fromDirection) {
		return edges.get(fromDirection) == Edge.OPEN;
	}

	@Override
	public void draw(float x, float y) {
		Game.inst().pushMatrix();
		translateForRotation(x, y);
		Game.inst().rotate(Game.radians(rotationDegrees));
		Game.inst().image(Game.imageManager().getImage(imageName), 0, 0, Game.pixelsPerCell, Game.pixelsPerCell);
		Game.inst().popMatrix();
	}
	
	private void translateForRotation(float x, float y) {
		float newX = x;
		float newY = y;
		
		switch(rotationDegrees) {
		case 90:
			newX = x + Game.pixelsPerCell;
			break;
		case 180:
			newX = x + Game.pixelsPerCell;
			newY = y + Game.pixelsPerCell;
			break;
		case 270:
			newY = y + Game.pixelsPerCell;
			break;
		}
		Game.inst().translate(newX, newY);
	}

	@Override
	public void rotateRight() {
		rotationDegrees += 90;
		rotationDegrees %= 360;
		HashMap<Integer, Edge> edgesCopy = new HashMap<Integer, Edge>(edges);
		edges.put(Game.RIGHT, edgesCopy.get(Game.UP));
		edges.put(Game.DOWN, edgesCopy.get(Game.RIGHT));
		edges.put(Game.LEFT, edgesCopy.get(Game.DOWN));
		edges.put(Game.UP, edgesCopy.get(Game.LEFT));	
	}
	
	@Override
	public void rotateLeft() {
		rotationDegrees += 270;
		rotationDegrees %= 360;
		HashMap<Integer, Edge> edgesCopy = new HashMap<Integer, Edge>(edges);
		edges.put(Game.RIGHT, edgesCopy.get(Game.DOWN));
		edges.put(Game.DOWN, edgesCopy.get(Game.LEFT));
		edges.put(Game.LEFT, edgesCopy.get(Game.UP));
		edges.put(Game.UP, edgesCopy.get(Game.RIGHT));
	}

}
