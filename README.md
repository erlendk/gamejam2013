HOW TO:
=======

A JAR file is available on the Download page, or it can be built from the 
source:


Clone the repository
--------------------

    $ git clone git@bitbucket.org:HrKristiansen/gamejam2013.git


Enter the new directory and build jar file
------------------------------------------

    $ cd gamejam2013
    $ ant jar


Run the game
------------

    $ java -jar game.jar

